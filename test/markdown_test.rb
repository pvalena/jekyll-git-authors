require_relative './test_helper'
require 'jekyll-git-authors/markdown'

class TestMarkdown < Minitest::Test
  def test_markdown_mailto
    author, email = "Your Name", "you@example.com"
    assert_match "[#{author}](mailto:{{ '#{email}' | encode_email }})", Markdown.mailto(author, email)
  end

  def test_markdown_center
    text = 'foo'
    assert_match %Q|{:center: style="text-align: center"}\n#{text}\n{:center}|, Markdown.center(text)
  end
end
