require "fileutils"

require_relative './test_helper.rb'
require 'jekyll-git-authors.rb'

class TestGit < Minitest::Test
  def setup
    @original_dir = Dir.pwd
    @dir = Dir.mktmpdir 'jekyll-git-authors'

    @subdir = File.join(@dir, 'subdir')

    Dir.mkdir @subdir

    FileUtils.cp './test/fixtures/README.md', @subdir

    Dir.chdir @dir


    Git.execute 'init'

    Git.execute 'config user.email "you@example.com"'
    Git.execute 'config user.name "Your Name"'

    Git.execute 'add .'
    Git.execute 'commit -m "Initial commit"'
  end

  def teardown
    Dir.chdir @original_dir
    FileUtils.rm_rf @dir
  end

  def test_that_git_executes_basic_commands
    assert_match /^git version /, Git.execute('--version')
  end

  def test_that_git_aborts_on_invalid_command
    invalid_command = 'patses'
    err_message = assert_raises(SystemExit) do
      Git.execute invalid_command
    end

    assert_equal "Unknown git command: '#{invalid_command}'", "#{err_message}"
  end

  def test_that_submodules_extracts_paths
    command_out = "Entering 'content'\n#{Dir.pwd}/content"
    git_submodules = Git::Submodules.new command_out
    expected_content = ["content"]

    assert_equal( expected_content, git_submodules.extract_path )
  end

  def test_git_log
    method_output = Git.log File.join(@subdir, 'README.md')
    assert_equal "Your Name;you@example.com", "#{method_output}"
  end

  def test_latest_author_email_is_used
    output = "Your Name;you@example.com\nYour Name;me@example.com"
    git_log = Git::Log.new output
    assert_equal({"Your Name"=>"you@example.com"}, git_log.author_email)
  end
end
