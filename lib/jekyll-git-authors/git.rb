module Git
  # Git::Log parses output passed from Git#log and creates a hash with author as a key and email as value
  class Log
    # We are expecting output from "git log" command
    def initialize(log)
      @log = log
    end

    # Iterate through each line in the log
    # filter out whitespace characters, split the line by semicolon
    # assign the returned pair (i.e. author and email) to author and email variables
    # then create hash with author as key and email as value and return it.
    def author_email
      author_email = Hash.new
      @log.each_line do |line|
        author, mail = line.strip.split(';')

        author_email[author] = mail unless author_email.key?(author)
      end
      author_email
    end

    # To ensure log will be text when we want
    def to_s
      @log
    end
  end

  # Git::Submodules parses output from Git#submodules and creates an array of submodules
  class Submodules
    # Expected input is "git submodule foreach 'echo $path'".
    # That command gives us paths relative to current directory.
    def initialize(paths)
      @paths = paths
    end

    # Clean the @path from redundant text that git generates.
    # First we take the @paths. We iterate through each line,
    # ignore ballast git puts there 'Entering ...'
    # and make it a path relative to the current directory
    # that git puts there, we then add the absolute path into an array.
    def extract_path
      submodules = Array.new
      @paths.each_line do |line|
        unless line =~ /Entering/
          line.strip!
          line.sub!(/^#{Dir.pwd}\//, '')

          submodules << line
        end
      end
      submodules
    end
  end

  # Take a git command and execute
  # Abort if command didn't execute successfully
  # then return output of the command.
  def self.execute(command)
    output = `git #{command}`

    abort "Unknown git command: '#{command}'" unless $?.success?

    return output
  end

  # Create instance of Git::Submodules and execute expected command.
  # We receive array of absolute paths of all submodules
  def self.submodules
    Submodules.new(execute('submodule foreach pwd'))
  end

  # Create instance of Git::Log and pass it output from git log command
  # return 5 last authors of a particular file
  # %an returns author, semicolon for effortless parsing, %ae return email of author
  def self.log(file)
    Log.new(execute("log -5 --pretty=format:'%an;%ae' #{file}"))
  end
end
