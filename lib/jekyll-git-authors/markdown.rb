module Markdown
  def self.mailto(a, e)
    "[#{a}](mailto:{{ '#{e}' | encode_email }})"
  end

  def self.center(text)
    '{:center: style="text-align: center"}' + "\n#{text}\n{:center}"
  end
end
