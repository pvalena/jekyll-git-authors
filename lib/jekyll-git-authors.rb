require 'jekyll-git-authors/version'
require 'jekyll-git-authors/markdown'
require 'jekyll-git-authors/git'

require 'jekyll'

# This script gets last 5 authors of a file and writes them at the end of the file.

module Jekyll
  class AuthorsGenerator < Generator
    safe true
    priority :high

    # iterate through pages then pick those which are
    # are markdown file then call Git#log on the page
    # and add authors into its content
    def generate(site)
      puts "\nGenerating authors"

      repositories = Git.submodules.extract_path

      # For each submodule we go into directory of that submodule
      # delete the submodule dir from page path
      # before executing Git#log, then go back where we were.
      site.pages.each do |page|
        file = page.path
        if file =~ /\.md/
          repositories.each do |repository|
            if file =~ /^#{repository}/
              original_dir = Dir.pwd
              Dir.chdir repository

              file.sub!(repository, '.')

              author_md = Git.log(file).author_email
              author_md = author_md.sort
              author_md = author_md.map { |a, e| Markdown.mailto(a, e) }

              output = author_md.join ', '
              output = "Authors: #{output}"
              output = Markdown.center output

              page.content += output

              Dir.chdir original_dir
            else
              author_md = Git.log(file).author_email
              author_md = author_md.sort
              author_md = author_md.map { |a, e| Markdown.mailto(a, e) }

              output = author_md.join ', '
              output = "Authors: #{output}"
              output = Markdown.center output

              page.content += output
            end
          end
        end
      end
    end
  end
end
