
lib = File.expand_path("../lib", __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require "jekyll-git-authors/version"

Gem::Specification.new do |spec|
  spec.name          = "jekyll-git-authors"
  spec.version       = JekyllGitAuthors::VERSION
  spec.authors       = ["Jaroslav Prokop"]
  spec.email         = ["jar.prokop@volny.cz"]

  spec.summary       = %q{Jekyll plugin that adds git authors into your page.}
  spec.description   = %q{Jekyll plugin that adds git authors and their obfuscated email address into page using markdown and liquid.}
  spec.homepage      = "https://gitlab.com/jackorp/jekyll-git-authors"
  spec.license       = "MIT"

  # Prevent pushing this gem to RubyGems.org. To allow pushes either set the 'allowed_push_host'
  # to allow pushing to a single host or delete this section to allow pushing to any host.
  if spec.respond_to?(:metadata)
    spec.metadata["allowed_push_host"] = 'https://rubygems.org'
  else
    raise "RubyGems 2.0 or newer is required to protect against " \
      "public gem pushes."
  end

  spec.files         = `git ls-files -z`.split("\x0").reject do |f|
    f.match(%r{^(test|spec|features)/})
  end
  spec.require_paths = ["lib"]

  spec.add_dependency "jekyll"
  spec.add_dependency "jekyll-email-protect"

  spec.add_development_dependency "simplecov"
  spec.add_development_dependency "bundler", "~> 1.16"
  spec.add_development_dependency "rake", "~> 10.0"
  spec.add_development_dependency "minitest"
end
